import { Button, Checkbox, Form, Input, message } from 'antd';
import NextIcon from 'asset/icons/NextIcon';
import axios from 'axios';
import { email, password, TOKEN } from 'common/patterns';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './login.scss';

export interface IUser {
  email: string;
  password: string;
  remember: boolean;
}

const Login = () => {
  useEffect(() => {}, []);
  const navigate = useNavigate();
  const onFinish = (results: IUser) => {
    if (results) {
      (async () => {
        try {
          const response = await axios({
            method: 'post',
            url: 'http://localhost:4001/auth/login',
            data: {
              email: results.email,
              password: results.password
            }
          });
          if (response.data.msg === 'success') {
            message.success('Login successfully');

            if (response.data.secretData.token) {
              localStorage.setItem(TOKEN, response.data.secretData.token);
              navigate('/dashboard');
            }
          }
        } catch (error) {
          message.error('Email or password incorrect, please check again');
        }
      })();
    }
  };

  const requestAccountHandler = () => {
    navigate('/register');
  };
  const onFinishFailed = () => {};
  return (
    <div className="login-component">
      <div className="login-component__wrapper gx-d-flex">
        <Form
          name="basic"
          layout="vertical"
          initialValues={{
            remember: true
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          className="login-component__wrapper-layout "
        >
          <div className="wrapper-layout__title">Đăng nhập</div>
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: 'Please input your email!'
              },
              {
                pattern: email,
                message: 'nhap dung email'
              }
            ]}
          >
            <Input placeholder="Email" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!'
              },
              {
                pattern: password,
                message:
                  'Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character'
              }
            ]}
          >
            <Input.Password placeholder="Password" />
          </Form.Item>

          <div className="wrapper-login-button">
            <Form.Item name="remember" valuePropName="checked">
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit" className="btn-submit">
                Login
              </Button>
            </Form.Item>
          </div>

          <div className="request-a-account" onClick={requestAccountHandler}>
            <div className="request-a-account__text">request an account</div>
            <div className="request-a-account__icon">
              <NextIcon />
            </div>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Login;
