import { Button, Form, Input } from 'antd';
import { useNavigate } from 'react-router-dom';

const Add = () => {
  const navigate = useNavigate();
  const onFinish = (results: any) => {};

  const cancelHandler = () => {
    navigate(-1);
  };
  return (
    <div>
      <Form
        name="basic"
        layout="vertical"
        initialValues={{
          remember: true
        }}
        onFinish={onFinish}
        autoComplete="off"
        className="login-component__wrapper-layout "
      >
        <div className="wrapper-layout__title">Đăng nhập</div>
        <Form.Item
          label="title"
          name="title"
          rules={[
            {
              required: true,
              message: 'Please input your email!'
            }
          ]}
        >
          <Input placeholder="Enter title" />
        </Form.Item>

        <Form.Item
          label="content"
          name="content"
          rules={[
            {
              required: true,
              message: 'Please input your password!'
            }
          ]}
        >
          <Input placeholder="enter content" />
        </Form.Item>
        <Form.Item>
          <div className="gx-d-flex-only gx-justify-space-between">
            <Button type="primary" htmlType="submit" className="btn-submit">
              post
            </Button>
            <Button
              type="ghost"
              htmlType="button"
              className="btn-cancel"
              onClick={cancelHandler}
            >
              cancel
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Add;
