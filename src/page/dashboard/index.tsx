import { IconHeart } from 'asset/icons/IconHeart';
import axios from 'axios';
import { TOKEN } from 'common/patterns';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import cls from 'classnames';
import './dashboard.scss';

const Dashboard = () => {
  const [fillColor, setFillColor] = useState(false);

  useEffect(() => {
    const getToken = localStorage.getItem(TOKEN);
    if (!getToken) navigate('/login');
  }, []);
  const navigate = useNavigate();

  const onClickAddHandle = () => {
    navigate('/dashboard/add');
  };

  const onClickEditHandle = () => {
    navigate('/dashboard/edit', { state: { id: 7, color: 'green' } });
  };

  const clickHeartHandler = () => {
    setFillColor(!fillColor);
  };

  const logoutHandle = () => {
    (async () => {
      try {
        const response = await axios({
          method: 'get',
          url: 'http://localhost:4001/auth/logout'
        });
        if (response.status === 200) {
          localStorage.removeItem(TOKEN);
          navigate('/login');
        }
      } catch (error) {
        console.log(error);
      }
    })();
  };
  return (
    <div className="dashboard-wrapper">
      <div className="logout">
        <button onClick={logoutHandle}>logout</button>
      </div>

      <div className="controller">
        <button onClick={onClickAddHandle}>Add</button>
      </div>
      <div className="content">
        <div className="gx-d-flex-only gx-justify-space-between">
          <div className="title">day la title bai viet</div>
          <button onClick={onClickEditHandle}>edit</button>
        </div>
        <div className="post-by">tac gia</div>
        <div className="description">day la noi dung bai viet</div>
        <span
          className={cls('pointer', {
            ['fill-color-red']: fillColor
          })}
          onClick={clickHeartHandler}
        >
          <IconHeart />
        </span>
      </div>
    </div>
  );
};

export default Dashboard;
