import { httpClient } from '@util/api';
import { Button, Checkbox, Form, Input } from 'antd';
import NextIcon from 'asset/icons/NextIcon';
import axios from 'axios';
import { email, password } from 'common/patterns';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import '../login/login.scss';

export interface IDataSubmit {
  firstName?: string;
  lastName?: string;
  email: string;
  password: string;
  confirmPassword: string;
}

const Register = () => {
  const navigate = useNavigate();
  const [checkPasswordConfirm, setCheckPasswordConfirm] = useState({
    password: '',
    confirmPassword: ''
  });

  // useEffect(() => {
  //   axios({
  //     method: 'post',
  //     url: 'http://localhost:4001/users/registration',
  //     data: {
  //       firstName: 'anh',
  //       lastName: 'tuan',
  //       email: 'vhaovvvx123@gmail.com',
  //       password: '123456aA@',
  //       confirmPassword: '123456aA@'
  //     }
  //   });
  // }, []);
  const backToLogin = () => {
    navigate('/login');
  };

  const handleChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const getValue = e.target.value;
    console.log(getValue);
  };
  const onFinish = (dataFinish: IDataSubmit) => {
    console.log('11', dataFinish);
  };
  const onFinishFailed = () => {};

  return (
    <div className="login-component">
      <div className="login-component__wrapper gx-d-flex">
        <Form
          name="basic"
          layout="vertical"
          initialValues={{
            remember: true
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          className="login-component__wrapper-layout "
        >
          <div className="wrapper-layout__title"> Become a membeer</div>

          <Form.Item label="First Name" name="firstName">
            <Input placeholder="Enter First Name" />
          </Form.Item>

          <Form.Item label="Last Name" name="lastName">
            <Input placeholder="Enter Last Name" />
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: 'Please input your email!'
              },
              {
                pattern: email,
                message: 'nhap dung email'
              }
            ]}
          >
            <Input placeholder="Enter email" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!'
              },
              {
                pattern: password,
                message:
                  'Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character'
              }
            ]}
          >
            <Input.Password
              placeholder="Password"
              onChange={handleChangeInput}
            />
          </Form.Item>

          <Form.Item
            label="Confirm Password"
            name="confirmPassword"
            rules={[
              {
                required: true,
                message: 'Please input your password!'
              },
              {
                pattern: password,
                message:
                  'Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character'
              }
            ]}
          >
            <Input.Password
              placeholder="Confirm Password"
              onChange={handleChangeInput}
            />
          </Form.Item>

          <div className="">
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="btn-submit-register"
              >
                Đăng ký
              </Button>

              <div className="request-a-account" onClick={backToLogin}>
                <div className="request-a-account__text">BACK TO LOGIN</div>
                <div className="request-a-account__icon">
                  <NextIcon />
                </div>
              </div>
            </Form.Item>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Register;
