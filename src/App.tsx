import React, { useEffect } from 'react';
import InputComponent from './components/input';
import Login from './page/login';
import { Route, Routes, useNavigate } from 'react-router-dom';
import Register from 'page/register/register';
import Dashboard from 'page/dashboard';
import Add from 'page/add';
import { Edit } from 'page/edit';

function App() {
  const navigate = useNavigate();

  useEffect(() => {
    const getToken = localStorage.getItem('token');
    if (!getToken) {
      navigate('/login');
    } else {
      navigate('/dashboard');
    }
  }, []);

  return (
    <div className="App">
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/dashboard/add" element={<Add />} />
        <Route path="/dashboard/edit" element={<Edit />} />
      </Routes>
    </div>
  );
}

export default App;
