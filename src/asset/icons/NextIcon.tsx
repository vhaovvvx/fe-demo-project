export const NextIcon = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M15 6L21 12L15 18" stroke="#596481" strokeWidth="2" />
      <path
        opacity="0.72"
        d="M9 6L15 12L9 18"
        stroke="#596481"
        strokeWidth="2"
      />
      <path
        opacity="0.48"
        d="M3 6L9 12L3 18"
        stroke="#596481"
        strokeWidth="2"
      />
    </svg>
  );
};

export default NextIcon;
